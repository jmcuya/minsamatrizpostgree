package dipos.ris.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.CondLaboral;
import dipos.ris.api.repo.CondLaboralRepo;

@Service
@Transactional
public class CondLaboralServiceImpl implements CondLaboralService{

	@Autowired
	private CondLaboralRepo condLaboralRepo;
	
	@Override
	public List<CondLaboral> listarCondLaborales() {
		return (List<CondLaboral>) condLaboralRepo.findAll();
	}

	@Override
	public CondLaboral obtenerCondLaboral(int id) {
		return condLaboralRepo.findById(id).get();
	}
	
	@Override
	public CondLaboral grabarCondLaboral(CondLaboral conlab) {
		return condLaboralRepo.save(conlab);
	}

	@Override
	public boolean condLaboralExiste(int id) {		
		return condLaboralRepo.existsById(id);
	}

}
