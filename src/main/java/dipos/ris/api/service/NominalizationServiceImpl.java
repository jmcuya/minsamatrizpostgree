package dipos.ris.api.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.NominalizationLegacy;
import dipos.ris.api.request.NominalizationRequest;

@Service
@Transactional
public class NominalizationServiceImpl implements NominalizationService{

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
	@Override
    public List<NominalizationLegacy> getNominalization(NominalizationRequest nominalization, String type) {
        String query = "FROM NominalizationLegacy  n where 1=1 ";
        String queryRis = "";
        String queryByDNI = "";
        String queryByNombres = "";
        String queryByPaterno = "";
        String queryByMaterno = "";
        String queryByGenero = "";
        String queryByEdad = "";
        String ris = nominalization.getRis()!=null?nominalization.getRis().getValue():"";
        String id_ris = nominalization.getRis()!=null?nominalization.getRis().getValue():"";
        String dni = nominalization.getDni()!=null?nominalization.getDni():"";
        String nombres = nominalization.getNombres()!=null?nominalization.getNombres():"";
        String paterno = nominalization.getPaterno()!=null?nominalization.getPaterno():"";
        String materno = nominalization.getMaterno()!=null?nominalization.getMaterno():"";
        String genero = nominalization.getGenero()!=null?nominalization.getGenero():"";
        String edad = nominalization.getEdad()!=null?nominalization.getEdad():"";

        System.out.println(dni);

        if(nominalization.getRis() != null) {
            if(type.equals("1")){
                queryRis = " and n.id_ris = :filter";

            }else {
                queryRis = " and n.ris like concat('%',:filter,'%') ";
            }
        } else {
            if(dni != "") {
                queryByDNI = " and n.dni = :dni";
            }
            if(nombres != "") {
                queryByNombres = " and n.nombres_ref = :nombres";
            }
            if(paterno != "") {
                queryByPaterno = " and n.paterno_ref = :paterno";
            }
            if(materno != "") {
                queryByMaterno = " and n.materno_ref = :materno";
            }
            if(genero != "") {
                queryByGenero = " and n.genero = :genero";
            }
            if(edad != "") {
                queryByEdad = " and n.edad = :edad";
            }
        }

        query = query + queryRis + queryByDNI + queryByNombres + queryByPaterno + queryByMaterno + queryByGenero + queryByEdad;

        Query query1 = entityManager.createQuery(query);

        if(queryRis!=""){
            query1.setParameter("filter", type.equals("1")?ris:id_ris);
            query1.setFirstResult(1).setMaxResults(100);
        }
        if(queryByDNI!=""){
            query1.setParameter("dni", dni!=null?dni:"");
        }
        if(queryByNombres!=""){
            query1.setParameter("nombres", nombres!=null?nombres:"");
        }
        if(queryByPaterno!=""){
            query1.setParameter("paterno", paterno!=null?paterno:"");
        }
        if(queryByMaterno!=""){
            query1.setParameter("materno", materno!=null?materno:"");
        }
        if(queryByGenero!=""){
            query1.setParameter("genero", genero!=null?genero.equals("1")?"HOMBRE":"MUJER":"");
        }
        if(queryByEdad!=""){
            query1.setParameter("edad", edad!=null?edad:"");
        }

        List<NominalizationLegacy> lista = query1
                .getResultList();
        System.out.println(lista);
        if(lista.isEmpty()) {
            return lista;
        }
        return lista;
    }
}
