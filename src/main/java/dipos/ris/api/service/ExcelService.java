package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.Tarea;

public interface ExcelService {
    List<Tarea> listarTareasUsuario(int idUsuario, int anio);
}
