package dipos.ris.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.DirecGeneral;
import dipos.ris.api.repo.DirecGeneralRepo;

@Service
@Transactional
public class DirecGeneralServiceImpl implements DirecGeneralService{

	@Autowired
	private DirecGeneralRepo direcGeneralRepo;
	
	@Override
	public List<DirecGeneral> listarDirecGenerales() {
		return (List<DirecGeneral>) direcGeneralRepo.findAll();
	}

	@Override
	public DirecGeneral obtenerDirecGeneral(int id) {
		return direcGeneralRepo.findById(id).get();
	}

	@Override
	public DirecGeneral grabarDirecGeneral(DirecGeneral dirgen) {
		return direcGeneralRepo.save(dirgen);
	}

	@Override
	public boolean direcGeneralExiste(int id) {		
		return direcGeneralRepo.existsById(id);
	}

}
