package dipos.ris.api.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dipos.ris.api.domain.Tarea;
import dipos.ris.api.repo.TareaRepo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExcelServiceImpl implements ExcelService {

	@Autowired
	private TareaRepo tareaRepo;
	
	@Override
	public List<Tarea> listarTareasUsuario(int idUsuario, int anio) {
		 List<Tarea> listaTarea = new ArrayList<Tarea>();
		try {
			listaTarea = tareaRepo.listarTareasUsuario(idUsuario, anio);
		} catch (Exception e) {
			log.error("listarTareasUsurio Error: " + ExceptionUtils.getFullStackTrace(e)
			);
		}

		return listaTarea;
	}

}
