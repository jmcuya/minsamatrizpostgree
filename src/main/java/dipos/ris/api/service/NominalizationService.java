package dipos.ris.api.service;

import dipos.ris.api.domain.NominalizationLegacy;
import dipos.ris.api.request.NominalizationRequest;

import java.util.List;

public interface NominalizationService {
    List<NominalizationLegacy> getNominalization(NominalizationRequest nominalization, String type);
}
