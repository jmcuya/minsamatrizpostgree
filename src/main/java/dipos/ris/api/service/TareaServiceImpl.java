package dipos.ris.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.Tarea;
import dipos.ris.api.repo.TareaRepo;

@Service
@Transactional
public class TareaServiceImpl implements TareaService{

	@Autowired
	private TareaRepo tareaRepo;

	@Override
	public List<Tarea> listarTareas() {
		List<Tarea> tareaUsuario= new ArrayList<Tarea>();
		return tareaUsuario = tareaRepo.findAll();
	}

	@Override
	public Tarea grabarTarea(Tarea tar) {
		return tareaRepo.save(tar);
	}

	@Override
	public boolean tareaExiste(int id) {		
		return tareaRepo.existsById(id);
	}

}
