package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.TipoDocumento;

public interface TipoDocumentoService {

	public List<TipoDocumento> listarTipoDocumentos();
	public TipoDocumento obtenerTipoDocumento(int id);
	public TipoDocumento grabarTipoDocumento(TipoDocumento tipdoc);
	public boolean tipoDocumentoExiste(int id);
	
}
