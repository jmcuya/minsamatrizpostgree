package dipos.ris.api.service;


import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.io.BaseEncoding;
import com.google.common.io.ByteSource;
import com.google.common.io.Files;

import dipos.ris.api.domain.Evidencia;
import dipos.ris.api.domain.Meta;
import dipos.ris.api.domain.Tarea;
import dipos.ris.api.domain.TipoDocEvidencia;
import dipos.ris.api.domain.Usuario;
import dipos.ris.api.repo.EvidenciaRepo;
import dipos.ris.api.repo.MetaRepo;
import dipos.ris.api.repo.TareaRepo;
import dipos.ris.api.repo.TipoDocEvidenciaRepo;
import dipos.ris.api.repo.UsuarioRepo;
import dipos.ris.api.request.EvidenciaRequest;
import dipos.ris.api.request.MetaRequest;
import dipos.ris.api.request.MetaRequestSave;
import dipos.ris.api.request.ValidaTareaRequest;
import dipos.ris.api.response.BandejaMetaResponse;
import dipos.ris.api.response.BandejaRegistrosMetaResponse;
import dipos.ris.api.response.EvidenciaResponse;
import dipos.ris.api.response.MetaAnualResponse;
import dipos.ris.api.response.MetaResponse;
import dipos.ris.api.response.MetasTrimestresResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class MetaServiceImpl implements MetaService{
	
	@Autowired
	private MetaRepo metaRepo;
	@Autowired
	private TareaRepo tareaRepo;
	@Autowired
	private UsuarioRepo usuarioRepo;
	@Autowired
	private TipoDocEvidenciaRepo tipoDocEvidenciaRepo;
	@Autowired
	private EvidenciaRepo evidenciaRepo;
	
	//@Value("${sistema.ruta.archivos}")
	private String rutaArchivos ="C:/archivosSistemas/";
	
	@Override
	public boolean metaExiste(int idUsuario, String anio) {
		boolean existe=false;
		
		return existe;
	}
	
	@Override
	public MetaResponse grabarMeta(MetaRequestSave req) {
		Meta meta = new Meta();
		MetaResponse metaResponse= new MetaResponse();
		try {
			Tarea tarea = tareaRepo.findById(req.getIdTarea()).get();
			Usuario usu = usuarioRepo.findById(req.getIdUsuario()).get();
			BeanUtils.copyProperties(req, meta);
			meta.setUsuario(usu);
			meta.setTarea(tarea);
			
			Meta metaSave = metaRepo.save(meta);
			if(metaSave==null) {
				return null;
			}
			Evidencia evid = new Evidencia();
			EvidenciaRequest eviReq = req.getEvidenciaRequest();
			TipoDocEvidencia tde = tipoDocEvidenciaRepo.findById(req.getEvidenciaRequest().getIdTipoDocEvidencia()).get();
			evid.setTipoDocEvidencia(tde);
			evid.setMeta(metaSave);
			ZoneId defaultZoneId = ZoneId.systemDefault();
			LocalDate date = LocalDate.parse(eviReq.getFechaEmision());
			evid.setFechaEmision(Date.from(date.atStartOfDay(defaultZoneId).toInstant()));
			String nombrePdf = "id_"+req.getIdUsuario()+"_"+UUID.randomUUID().toString().substring(0, 8) + ".pdf";
			String rutaPdf = rutaArchivos +nombrePdf;
							
			File miPdf = new File(rutaPdf);
			if(miPdf.createNewFile()) {
				byte[] pdfEvidencia = Base64.getDecoder().decode(eviReq.getFileBase64());
				if(miPdf.exists())
					Files.write(pdfEvidencia, miPdf);
				evid.setFileUrl(rutaPdf);
				
				Evidencia evisave= evidenciaRepo.save(evid);
				if(evisave==null)
					return null;
				BeanUtils.copyProperties(meta, metaResponse);
				
			}else {
				return null;
			}
			
			
		} catch (Exception e) {
			log.error("error: "+ ExceptionUtils.getFullStackTrace(e));
		}
		return metaResponse;
	}
	

	
	@Override
	public MetaAnualResponse listaMetas(MetaRequest req) {
		
		MetaAnualResponse response = new MetaAnualResponse();
		List<Meta> listaMetas =metaRepo.findByUsuarioIdUsuarioAndAnio(req.getIdUsuario(), req.getAnio());
		List<MetaResponse> listaMetasResponse = new ArrayList<MetaResponse>();
		listaMetas.forEach(m->{
			MetaResponse me = new MetaResponse();
			BeanUtils.copyProperties(m, me);
			listaMetasResponse.add(me);
		});
		
		List<MetasTrimestresResponse> listadoTrimestre = this.obtenerListadoTrimestres(listaMetas);
		
		response.setListaMetasTrimestreResponse(listadoTrimestre);
		response.setListaMetaResponse(listaMetasResponse);
		
		return response;
	}
	
	@Override
	public BandejaRegistrosMetaResponse listaBandejaMetas(MetaRequest req) {

		BandejaRegistrosMetaResponse response = new BandejaRegistrosMetaResponse();
		List<Meta> listaMetas =metaRepo.findByUsuarioIdUsuarioAndAnio(req.getIdUsuario(), req.getAnio());
		List<BandejaMetaResponse> listaBandejaMetasResponse = new ArrayList<BandejaMetaResponse>();
		listaMetas.forEach(m->{
			BandejaMetaResponse me = new BandejaMetaResponse();
			BeanUtils.copyProperties(m, me);
			Evidencia evi = evidenciaRepo.findByMetaIdMeta(m.getIdMeta());
			EvidenciaResponse eviRes = new EvidenciaResponse();
			if(evi!=null) {
				BeanUtils.copyProperties(evi,eviRes);
				String ruta = evi.getFileUrl();
				File archivo = new File(ruta);
				ByteSource source = Files.asByteSource(archivo);
			    byte[] file=null;
				try {
					file = source.read();
				} catch (IOException e) {
					log.error("error leyendo archivo: " + ExceptionUtils.getFullStackTrace(e));
				}
				String mFileBase64 = new String(BaseEncoding.base64().encode(file));
				eviRes.setFileBase64(mFileBase64);
				me.setEvidenciaResponse(eviRes);
			}
			listaBandejaMetasResponse.add(me);
		});
		
		List<MetasTrimestresResponse> listadoTrimestre = this.obtenerListadoTrimestres(listaMetas);
		
		response.setListaMetasTrimestreResponse(listadoTrimestre);
		response.setListaBandejaMetaResponse(listaBandejaMetasResponse);
		
		return response;
		
	}
	
	@Override
	public MetaResponse validarTarea(ValidaTareaRequest req) {
		MetaResponse metaResponse= new MetaResponse();
		try {
						
			Meta meta = metaRepo.findById(req.getIdMeta()).get();
			if(meta==null) {
				return null;
			}
			meta.setMotivoRechazo(req.getMotivo());
			meta.setEstado(String.valueOf(req.getEstado()));
			
			Meta metasave = metaRepo.findById(req.getIdMeta()).get();
			
			if(metasave!=null)
				BeanUtils.copyProperties(metasave, metaResponse);
			
		} catch (Exception e) {
			log.error("error validarTarea: "+ ExceptionUtils.getFullStackTrace(e));
		}
		return metaResponse;
	}


	private List<MetasTrimestresResponse> obtenerListadoTrimestres(List<Meta> listaMetas){
		
		List<MetasTrimestresResponse> listaMetasTrimestre= new ArrayList<MetasTrimestresResponse>();
		
		int ejecutados1 = listaMetas.stream().map(t->t.getValorTrim1Ejec()).collect(Collectors.summingInt(i->i));
		int ejecutados2 = listaMetas.stream().map(t->t.getValorTrim2Ejec()).collect(Collectors.summingInt(i->i));
		int ejecutados3 = listaMetas.stream().map(t->t.getValorTrim3Ejec()).collect(Collectors.summingInt(i->i));
		int ejecutados4 = listaMetas.stream().map(t->t.getValorTrim4Ejec()).collect(Collectors.summingInt(i->i));
		
		int programados1 = listaMetas.stream().map(t->t.getValorTrim1Prog()).collect(Collectors.summingInt(i->i));
		int programados2 = listaMetas.stream().map(t->t.getValorTrim2Prog()).collect(Collectors.summingInt(i->i));
		int programados3 = listaMetas.stream().map(t->t.getValorTrim3Prog()).collect(Collectors.summingInt(i->i));
		int programados4 = listaMetas.stream().map(t->t.getValorTrim4Prog()).collect(Collectors.summingInt(i->i));
		
		
		double valor1 = (double) ejecutados1/programados1;
		double valor2 = (double) ejecutados2/programados2;
		double valor3 = (double) ejecutados3/programados3;
		double valor4 = (double) ejecutados4/programados4;

		valor1 = Math.round(valor1*100.0)/100.0;
		valor2 = Math.round(valor2*100.0)/100.0;
		valor3 = Math.round(valor3*100.0)/100.0;
		valor4 = Math.round(valor4*100.0)/100.0;
		
		String porcentaje1 = Double.valueOf(valor1*100).toString()+"%"; 
		String porcentaje2 = Double.valueOf(valor2*100).toString()+"%"; 
		String porcentaje3 = Double.valueOf(valor3*100).toString()+"%"; 
		String porcentaje4 = Double.valueOf(valor4*100).toString()+"%"; 
		
		MetasTrimestresResponse mtr1 = new MetasTrimestresResponse();
		mtr1.setTrimestre("Trimestre I");
		mtr1.setEjecutado(String.valueOf(ejecutados1));
		mtr1.setProgramado(String.valueOf(programados1));
		mtr1.setValor(String.valueOf(valor1));
		mtr1.setPorcentaje(porcentaje1);
		
		MetasTrimestresResponse mtr2 = new MetasTrimestresResponse();
		mtr2.setTrimestre("Trimestre II");
		mtr2.setEjecutado(String.valueOf(ejecutados2));
		mtr2.setProgramado(String.valueOf(programados2));
		mtr2.setValor(String.valueOf(valor2));
		mtr2.setPorcentaje(porcentaje2);
		
		MetasTrimestresResponse mtr3 = new MetasTrimestresResponse();
		mtr3.setTrimestre("Trimestre III");
		mtr3.setEjecutado(String.valueOf(ejecutados3));
		mtr3.setProgramado(String.valueOf(programados3));
		mtr3.setValor(String.valueOf(valor3));
		mtr3.setPorcentaje(porcentaje3);
		
		MetasTrimestresResponse mtr4 = new MetasTrimestresResponse();
		mtr4.setTrimestre("Trimestre IV");
		mtr4.setEjecutado(String.valueOf(ejecutados4));
		mtr4.setProgramado(String.valueOf(programados4));
		mtr4.setValor(String.valueOf(valor4));
		mtr4.setPorcentaje(porcentaje4);
		
		listaMetasTrimestre.add(mtr1);
		listaMetasTrimestre.add(mtr2);
		listaMetasTrimestre.add(mtr3);
		listaMetasTrimestre.add(mtr4);
		
		return listaMetasTrimestre;
	}

	}
