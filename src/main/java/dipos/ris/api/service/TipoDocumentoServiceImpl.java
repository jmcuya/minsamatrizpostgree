package dipos.ris.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.TipoDocumento;
import dipos.ris.api.repo.TipoDocumentoRepo;

@Service
@Transactional
public class TipoDocumentoServiceImpl implements TipoDocumentoService{

	@Autowired
	private TipoDocumentoRepo tipoDocumentoRepo;
	
	@Override
	public List<TipoDocumento> listarTipoDocumentos() {
		return (List<TipoDocumento>) tipoDocumentoRepo.findAll();
	}

	@Override
	public TipoDocumento obtenerTipoDocumento(int id) {
		return tipoDocumentoRepo.findById(id).get();
	}

	@Override
	public TipoDocumento grabarTipoDocumento(TipoDocumento tipdoc) {
		return tipoDocumentoRepo.save(tipdoc);
	}

	@Override
	public boolean tipoDocumentoExiste(int id) {		
		return tipoDocumentoRepo.existsById(id);
	}

}
