package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.UniOrganica;

public interface UniOrganicaService {

	public List<UniOrganica> listarUniOrganicasByDirecGeneral(int id);
	public UniOrganica obtenerUniOrganica(int id);
	public UniOrganica grabarUniOrganica(UniOrganica uor);
	public boolean uniOrganicaExiste(int id);
	public List<UniOrganica> listarUniOrganicas();
	
}
