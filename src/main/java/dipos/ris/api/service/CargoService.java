package dipos.ris.api.service;

import java.util.List;

import dipos.ris.api.domain.Cargo;

public interface CargoService {

	public List<Cargo> listarCargos();
	public Cargo obtenerCargo(int id);
	public Cargo grabarCargo(Cargo car);
	public boolean cargoExiste(int id);
	
}
