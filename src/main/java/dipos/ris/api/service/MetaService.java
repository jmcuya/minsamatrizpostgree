package dipos.ris.api.service;

import dipos.ris.api.request.MetaRequest;
import dipos.ris.api.request.MetaRequestSave;
import dipos.ris.api.request.ValidaTareaRequest;
import dipos.ris.api.response.BandejaRegistrosMetaResponse;
import dipos.ris.api.response.MetaAnualResponse;
import dipos.ris.api.response.MetaResponse;

public interface MetaService {

	public boolean metaExiste(int idUsuario, String anio);

	public MetaResponse grabarMeta(MetaRequestSave req);
	
	public MetaResponse validarTarea(ValidaTareaRequest req);

	public MetaAnualResponse listaMetas(MetaRequest req);
	
	public BandejaRegistrosMetaResponse listaBandejaMetas(MetaRequest req);

}
