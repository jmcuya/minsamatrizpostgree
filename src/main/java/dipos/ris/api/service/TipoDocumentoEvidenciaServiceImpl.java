package dipos.ris.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.TipoDocEvidencia;
import dipos.ris.api.repo.TipoDocEvidenciaRepo;

@Service
@Transactional
public class TipoDocumentoEvidenciaServiceImpl implements TipoDocumentoEvidenciaService{

	@Autowired
	private TipoDocEvidenciaRepo tipoDocEvidenciaRepo;
	
	@Override
	public List<TipoDocEvidencia> listarTipoDocumentos() {
		return (List<TipoDocEvidencia>) tipoDocEvidenciaRepo.findAll();
	}

	@Override
	public TipoDocEvidencia obtenerTipoDocumento(int id) {
		return tipoDocEvidenciaRepo.findById(id).get();
	}

	@Override
	public TipoDocEvidencia grabarTipoDocumento(TipoDocEvidencia tipdoc) {
		return tipoDocEvidenciaRepo.save(tipdoc);
	}

	@Override
	public boolean tipoDocumentoExiste(int id) {		
		return tipoDocEvidenciaRepo.existsById(id);
	}

}
