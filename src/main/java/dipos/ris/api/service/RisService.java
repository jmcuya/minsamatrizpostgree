package dipos.ris.api.service;

import dipos.ris.api.domain.Ris;

import java.util.List;

public interface RisService {
    List<Ris> getListRis();
}
