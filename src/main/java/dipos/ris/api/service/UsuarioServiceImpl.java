package dipos.ris.api.service;


import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.CondLaboral;
import dipos.ris.api.domain.Role;
import dipos.ris.api.domain.Tarea;
import dipos.ris.api.domain.TareaUsuario;
import dipos.ris.api.domain.TipoDocumento;
import dipos.ris.api.domain.TipoUsuario;
import dipos.ris.api.domain.UniOrganica;
import dipos.ris.api.domain.Usuario;
import dipos.ris.api.repo.CargoRepo;
import dipos.ris.api.repo.CondLaboralRepo;
import dipos.ris.api.repo.RoleRepo;
import dipos.ris.api.repo.TareaRepo;
import dipos.ris.api.repo.TareaUsuarioRepo;
import dipos.ris.api.repo.TipoDocumentoRepo;
import dipos.ris.api.repo.TipoUsuarioRepo;
import dipos.ris.api.repo.UniOrganicaRepo;
import dipos.ris.api.repo.UsuarioRepo;
import dipos.ris.api.request.TareaRequest;
import dipos.ris.api.request.UsuarioRequest;
import dipos.ris.api.response.DevolverGrillaUsuariosDto;
import dipos.ris.api.response.UsuarioResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private UsuarioRepo usuarioRepo;
	@Autowired
	private TareaRepo tareaRepo;
	@Autowired
	private TareaUsuarioRepo tareaUsuarioRepo;
	@Autowired
	private TipoDocumentoRepo tipoDocumentoRepo;
//	@Autowired
//	private DirecGeneralRepo direcGeneralRepo;
	@Autowired
	private UniOrganicaRepo uniOrganicaRepo;
	@Autowired
	private CondLaboralRepo condLaboralRepo;
	@Autowired
	private CargoRepo cargoRepo;
	@Autowired
	private TipoUsuarioRepo tipoUsuarioRepo;
	@Autowired
	private RoleRepo roleRepo;
	
	@Override
	public List<UsuarioResponse> listarUsuarios() {
		List<UsuarioResponse> listaUsuarioResponse= new ArrayList<UsuarioResponse>();
		try {
			List<Usuario> listaUsuario = usuarioRepo.findAll();
			
			listaUsuario.forEach(u->{
				UsuarioResponse usu = new UsuarioResponse();
				BeanUtils.copyProperties(u, usu);
				List<Tarea> listaTarea = tareaRepo.listarTareasUsuario(u.getIdUsuario(),0);
				usu.setListadoTareas(listaTarea);
				listaUsuarioResponse.add(usu);
			});
			
		} catch (Exception e) {
			log.error("Error listarUsuarios: " + ExceptionUtils.getFullStackTrace(e));
		}
		
		return listaUsuarioResponse;
	}

	@Override
	public UsuarioResponse obtenerUsuario(int id) {
		Usuario usuario = usuarioRepo.findById(id).get();
		UsuarioResponse usuRes = new UsuarioResponse();
		BeanUtils.copyProperties(usuario, usuRes);
		return usuRes;
	}

	@Override
	public UsuarioResponse grabarUsuario(UsuarioRequest usu) {
		TipoDocumento td= tipoDocumentoRepo.findById(usu.getIdTipoDocumento()).get();
		UniOrganica uo = uniOrganicaRepo.findById(usu.getIdUniOrganica()).get();
		CondLaboral cl = condLaboralRepo.findById(usu.getIdCondLaboral()).get();
		Cargo ca = cargoRepo.findById(usu.getIdCargo()).get();
		Role r = roleRepo.findById(Long.valueOf(usu.getIdRole())).get();
		TipoUsuario tu = tipoUsuarioRepo.findById(usu.getIdTipoUsuario()).get();

		
		Usuario usuario = new Usuario();
		if(usu.getIdUsuario()>0) {
			usuario = usuarioRepo.findById(usu.getIdUsuario()).get();
			List<TareaUsuario> listaTareasUsuario = usuario.getListadoTareaUsuario();
			for (TareaUsuario tusu : listaTareasUsuario) {
				if(tusu.getAnio()==usu.getAnio()) {
					tusu.setEstado(0);
					tareaUsuarioRepo.save(tusu);
				}
			}
			
		}
		BeanUtils.copyProperties(usu, usuario);
		usuario.setCargo(ca);
		usuario.setCondLaboral(cl);
		usuario.setUniOrganica(uo);
		usuario.setTipoDocumento(td);
		usuario.setRole(r);
		usuario.setTipoUsuario(tu);
		usuario.setEstado(1);
		ZoneId defaultZoneId = ZoneId.systemDefault();
		LocalDate date = LocalDate.parse(usu.getFecNacimiento());
		usuario.setFecNacimiento(Date.from(date.atStartOfDay(defaultZoneId).toInstant()));
		int anio =date.getYear();
		List<TareaUsuario> listadoTareaUsuario = new ArrayList<TareaUsuario>();
		for (TareaRequest t : usu.getListadoTareas()) {
			Tarea ta = tareaRepo.findById(t.getIdTarea()).get();
			TareaUsuario tau = new TareaUsuario();
			tau.setUsuario(usuario);
			tau.setTarea(ta);
			
			tau.setAnio(usu.getAnio()==0?anio:usu.getAnio());
			listadoTareaUsuario.add(tau);
		}
		usuario.setListadoTareaUsuario(listadoTareaUsuario);
		
		Usuario usuarioSave = usuarioRepo.save(usuario);
		UsuarioResponse usuRes = new UsuarioResponse();
		BeanUtils.copyProperties(usuarioSave, usuRes);
		return usuRes;
	}

	@Override
	public boolean usuarioExiste(int id) {
		
		return usuarioRepo.existsById(id);
	}

	@Override
	public DevolverGrillaUsuariosDto listarGrillaUsuarios(UsuarioRequest req, Pageable pageable) {
		Page<Usuario> listaUsuario = null;
		
		listaUsuario = usuarioRepo.listarUsuariosBuscador( 
				req.getIdUniOrganica(), 
				req.getNombre(), 
				req.getApePaterno(),
				req.getApeMaterno(), 
				req.getIdTipoDocumento(), 
				req.getNroDocumento(), 
				req.getIdCargo(), 
				req.getIdCondLaboral(),
				pageable
				);
		DevolverGrillaUsuariosDto respuestaUsuarios = new DevolverGrillaUsuariosDto();
		
		int number = listaUsuario.getNumber();
        int numberOfElements = listaUsuario.getNumberOfElements();
        long totalElements = listaUsuario.getTotalElements();
        int totalPages = listaUsuario.getTotalPages();
        
        List<UsuarioResponse> listaUsuarioResponse= new ArrayList<UsuarioResponse>();
		listaUsuario.forEach(u->{
			UsuarioResponse usu = new UsuarioResponse();
			BeanUtils.copyProperties(u, usu);
			List<Tarea> listaTarea = tareaRepo.listarTareasUsuario(u.getIdUsuario(),0);
			usu.setListadoTareas(listaTarea);
			listaUsuarioResponse.add(usu);
		});
		
        respuestaUsuarios.setCantidadRegistroPagina(numberOfElements);
        respuestaUsuarios.setCantidadRegistros(totalElements);
        respuestaUsuarios.setListaUsuarioResponse(listaUsuarioResponse);;
        respuestaUsuarios.setNumeroPagina(number);
        respuestaUsuarios.setTotalPaginas(totalPages);

		return respuestaUsuarios;
	}
	
}
