package dipos.ris.api.response;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class MetaAnualResponse {
	
	List<MetasTrimestresResponse> listaMetasTrimestreResponse;
	List<MetaResponse> listaMetaResponse;

}
