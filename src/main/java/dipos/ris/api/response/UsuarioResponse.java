package dipos.ris.api.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import dipos.ris.api.domain.Cargo;
import dipos.ris.api.domain.CondLaboral;
import dipos.ris.api.domain.Role;
import dipos.ris.api.domain.Tarea;
import dipos.ris.api.domain.TipoDocumento;
import dipos.ris.api.domain.TipoUsuario;
import dipos.ris.api.domain.UniOrganica;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class UsuarioResponse implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4566804250516260730L;
	private int idUsuario;
    private String nroDocumento;
    private String nombre;
    private String apePaterno;
    private String apeMaterno;
    private Date fecNacimiento;
    private String sexo;
    private String usuario;
    private String password;
    private int estado;
    private Cargo cargo;
    private CondLaboral condLaboral;
    private UniOrganica uniOrganica;
    private TipoUsuario tipoUsuario;
    private TipoDocumento tipoDocumento;
    private Role role;
    private List<Tarea> listadoTareas;
}
