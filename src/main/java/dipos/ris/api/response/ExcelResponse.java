package dipos.ris.api.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class ExcelResponse {
	
	private String usuario;
	private String fileBase64;
	private boolean exito;
	private String mensaje;

}
