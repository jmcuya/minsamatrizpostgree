package dipos.ris.api.response;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class DevolverGrillaUsuariosDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6289045793545119415L;
	private long CantidadRegistros;
	private int numeroPagina;
	private int cantidadRegistroPagina;
	private int totalPaginas;
	
    private List<UsuarioResponse> listaUsuarioResponse;
}
