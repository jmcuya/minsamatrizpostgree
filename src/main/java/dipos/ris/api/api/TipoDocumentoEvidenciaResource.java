package dipos.ris.api.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dipos.ris.api.domain.Mensaje;
import dipos.ris.api.domain.TipoDocEvidencia;
import dipos.ris.api.service.TipoDocumentoEvidenciaService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/tipoDocumentoEvidencia")
@RequiredArgsConstructor
public class TipoDocumentoEvidenciaResource {
    private final TipoDocumentoEvidenciaService tipoDocumentoEvidenciaService;

    @GetMapping("/listarTipoDocumentoEvidencia")
    @ApiOperation(value = "Este metodo lista todos los tipos de documentos")
    public ResponseEntity<List<TipoDocEvidencia>>getTipoDocumentoes() {
        return ResponseEntity.ok().body(tipoDocumentoEvidenciaService.listarTipoDocumentos());
    }

    @PostMapping("/grabarTipoDocumentoEvidencia")
    @ApiOperation(value = "Este metodo Graba un tipo de documento")
    public ResponseEntity<TipoDocEvidencia>grabarTipoDocumento(@RequestBody TipoDocEvidencia tidoc) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/tipoDocumento/grabarTipoDocumento").toUriString());
        return ResponseEntity.created(uri).body(tipoDocumentoEvidenciaService.grabarTipoDocumento(tidoc));
    }
    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ApiOperation(value = "Este metodo obtiene un tipo de documento")
    @GetMapping(path = "/obtenerTipoDocumentoEvidenciaId/{id}")
	public ResponseEntity<TipoDocEvidencia> obtenerTipoDocumento(@PathVariable(name = "id") int id){
		if(!tipoDocumentoEvidenciaService.tipoDocumentoExiste(id)) 
			return new ResponseEntity(new Mensaje("No existe el tipo de documento"),HttpStatus.NOT_FOUND);
		
		TipoDocEvidencia tidoc = tipoDocumentoEvidenciaService.obtenerTipoDocumento(id);
		
		return new ResponseEntity<TipoDocEvidencia>(tidoc,HttpStatus.OK);
	}
		
	@PutMapping(path = "/actualizarTipoDocumentoEvidencia/{id}")
	public ResponseEntity<TipoDocEvidencia> actualizarTipoDocumento(@PathVariable(name = "id") int id, @RequestBody TipoDocEvidencia tidoc){
		if(!tipoDocumentoEvidenciaService.tipoDocumentoExiste(id))
			return new ResponseEntity<TipoDocEvidencia>(HttpStatus.NOT_FOUND);
		TipoDocEvidencia tipoDocumentoActualizado = tipoDocumentoEvidenciaService.obtenerTipoDocumento(id);
		BeanUtils.copyProperties(tidoc, tipoDocumentoActualizado);
		return new ResponseEntity<TipoDocEvidencia>(tidoc,HttpStatus.OK);	
		
	}
	   
}

