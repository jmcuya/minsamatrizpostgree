package dipos.ris.api.api;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dipos.ris.api.domain.NominalizationLegacy;
import dipos.ris.api.domain.Ris;
import dipos.ris.api.request.NominalizationRequest;
import dipos.ris.api.service.NominalizationService;
import dipos.ris.api.service.RisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Api("APIS de RIS")
public class RisResource {
    private final RisService risService;
    private final NominalizationService nominalizationService;

    @GetMapping("/ris")
    @ApiOperation(value = "Servicio para obtener ris", notes = "Retorna JSON de listado de ris.")
	public ResponseEntity<List<Ris>> getRis () {
        return ResponseEntity.ok().body(risService.getListRis());
    }

    @PostMapping("/nominalizacion")
    public ResponseEntity<List<NominalizationLegacy>> getNominalization (@RequestBody NominalizationRequest nominalization, @RequestParam String type) {
        return ResponseEntity.ok().body(nominalizationService.getNominalization(nominalization, type));
    }
}
