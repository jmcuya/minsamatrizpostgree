package dipos.ris.api.api;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFRegionUtil;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.io.BaseEncoding;
import com.google.common.io.Files;

import dipos.ris.api.domain.Tarea;
import dipos.ris.api.request.ExcelRequest;
import dipos.ris.api.response.ExcelResponse;
import dipos.ris.api.service.ExcelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/excel")
@Api("APIS de EXCEL")
public class ExcelResource {

	//@Value("${sistema.ruta.archivos}")
	private String rutaArchivos = "C:/archivosSistemas/";

	@Autowired
	private ExcelService excelService;

	@PostMapping("/generarExcelPlantilla")
	@ApiOperation(value = "Servicio para generar plantilla", notes = "Retorna JSON base 64 archivo.")
	public ResponseEntity<ExcelResponse> generarExcelPlantilla(@RequestBody ExcelRequest req) {
		ExcelResponse response = new ExcelResponse();

		try {
			List<Tarea> listaTarea = excelService.listarTareasUsuario(req.getIdUsuario(), req.getAnio());

			HSSFWorkbook workbook = this.crearFormatoExcelUsuario(listaTarea);
			String nombreXls = UUID.randomUUID().toString().substring(0, 5) + ".xls";
			FileOutputStream fileOut = null;
			try {
				fileOut = new FileOutputStream(rutaArchivos + nombreXls);
				workbook.write(fileOut);
				fileOut.close();
			} catch (Exception e) {
				log.error("Error creando archivo excel: " + ExceptionUtils.getFullStackTrace(e));
				response.setExito(false);
				response.setMensaje("Error creando archivo excel");
			}

			File miXls = new File(rutaArchivos + nombreXls);

			byte[] file = null;
			String mXlsBase64 = null;

			file = Files.toByteArray(miXls);
			mXlsBase64 = new String(BaseEncoding.base64().encode(file));

			log.info("termino de envio base 64");

			response.setFileBase64(mXlsBase64);
			response.setMensaje(nombreXls);

			// this.eliminarArchivosLocalesPDF(rutaDirectorioPDF + nombreXls);

		} catch (Exception e) {
			log.error("generarExcelPlantilla Error: " + ExceptionUtils.getFullStackTrace(e));
		}
		return ResponseEntity.ok().body(response);
	}

	private HSSFWorkbook crearFormatoExcelUsuario(List<Tarea> listadoTareaUsuario) {

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();
		workbook.setSheetName(0, "MATRIZ DE SEGUIMIENTO ");

		// estilos de la celdas
		CellStyle stylePrimerRow = workbook.createCellStyle();
		stylePrimerRow.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		stylePrimerRow.setFillPattern(CellStyle.SOLID_FOREGROUND);
		stylePrimerRow.setAlignment(CellStyle.ALIGN_CENTER);
		stylePrimerRow.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		stylePrimerRow.setWrapText(true);

		CellStyle styleCabeceras = workbook.createCellStyle();
		styleCabeceras.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		styleCabeceras.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleCabeceras.setAlignment(CellStyle.ALIGN_CENTER);
		styleCabeceras.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		styleCabeceras.setWrapText(true);
		
		CellStyle styleBorder = workbook.createCellStyle();
		styleBorder.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		styleBorder.setBorderBottom((short)1);
		styleBorder.setBorderLeft((short)1);
		styleBorder.setBorderRight((short)1);
		styleBorder.setBorderTop((short)1);
		styleBorder.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleBorder.setAlignment(CellStyle.ALIGN_CENTER);
		styleBorder.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		styleBorder.setWrapText(true);
		
		CellStyle styleMetas = workbook.createCellStyle();
		styleMetas.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
		styleMetas.setBorderBottom((short)1);
		styleMetas.setBorderLeft((short)1);
		styleMetas.setBorderRight((short)1);
		styleMetas.setBorderTop((short)1);
		styleMetas.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styleMetas.setAlignment(CellStyle.ALIGN_LEFT);
		styleMetas.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		styleMetas.setWrapText(true);
		
		
		// Primera Linea
		HSSFRow primerRow = sheet.createRow(0);
		// segunda Linea
		HSSFRow segundaRow = sheet.createRow(1);
		// segunda Linea
		HSSFRow terceroRow = sheet.createRow(2);
		// Primera Columna combianada ITEM
		HSSFCell celdaItem = primerRow.createCell(0);
		celdaItem.setCellValue("ITEM");
		CellRangeAddress tareaItem = new CellRangeAddress(0, 2, 0, 0);
		sheet.addMergedRegion(tareaItem);
		
		// SEGUNDA Columna combianada TAREA
		HSSFCell celdaTarea = primerRow.createCell(1);
		celdaTarea.setCellValue("TAREA");
		celdaTarea.setCellStyle(stylePrimerRow);
		CellRangeAddress tareaMerge = new CellRangeAddress(0, 2, 0, 1);
		sheet.addMergedRegion(tareaMerge);
		// tercera Columna META FISICA
		HSSFCell celdaMetaFisica = primerRow.createCell(2);
		celdaMetaFisica.setCellValue("PROGRAMACIÓN DE METAS FÍSICAS");
		CellRangeAddress metaFisicaMerge = new CellRangeAddress(0, 0, 2, 10);
		sheet.addMergedRegion(metaFisicaMerge);
		celdaMetaFisica.setCellStyle(styleCabeceras);
		// cuarta Columna programa presupuestal
		HSSFCell celdaPrograma = primerRow.createCell(11);
		celdaPrograma.setCellValue("PROGRAMACIÓN PRESUPUESTAL (S/.)");
		CellRangeAddress metaPrograma = new CellRangeAddress(0, 0, 11, 19);
		sheet.addMergedRegion(metaPrograma);
		celdaPrograma.setCellStyle(styleCabeceras);
		
		HSSFCell celdaMotivo = primerRow.createCell(20);
		celdaMotivo.setCellValue("Motivo del grado de avance de metas físicas y presupuestales");
		CellRangeAddress MotivoItem = new CellRangeAddress(0, 2, 20, 20);
		sheet.addMergedRegion(MotivoItem);
		celdaMotivo.setCellStyle(styleBorder);

		String[] trimestres = new String[] { "I trimestre", "II trimestre", "III trimestre", "IV trimestre" };
		int j = 2;
		for (int i = 0; i < trimestres.length; ++i) {
			String nombre = trimestres[i];
			HSSFCell cell = segundaRow.createCell(j);
			cell.setCellValue(nombre);
			cell.setCellStyle(styleCabeceras);
			j = j + 2;
		}
		HSSFCell cell = segundaRow.createCell(10);
		cell.setCellValue("Anual");
		cell.setCellStyle(styleBorder);

		j = 11;
		for (int i = 0; i < trimestres.length; ++i) {
			String nombre = trimestres[i];
			HSSFCell cell1 = segundaRow.createCell(j);
			cell1.setCellValue(nombre);
			cell1.setCellStyle(styleCabeceras);
			j = j + 2;
		}

		HSSFCell cell1 = segundaRow.createCell(19);
		cell1.setCellValue("Anual");
		cell1.setCellStyle(styleBorder);

		CellRangeAddress tri1F = new CellRangeAddress(1, 1, 2, 3);
		sheet.addMergedRegion(tri1F);
		CellRangeAddress tri2F = new CellRangeAddress(1, 1, 4, 5);
		sheet.addMergedRegion(tri2F);
		CellRangeAddress tri3F = new CellRangeAddress(1, 1, 6, 7);
		sheet.addMergedRegion(tri3F);
		CellRangeAddress tri4F = new CellRangeAddress(1, 1, 8, 9);
		sheet.addMergedRegion(tri4F);

		CellRangeAddress tri1P = new CellRangeAddress(1, 1, 11, 12);
		sheet.addMergedRegion(tri1P);
		CellRangeAddress tri2P = new CellRangeAddress(1, 1, 13, 14);
		sheet.addMergedRegion(tri2P);
		CellRangeAddress tri3P = new CellRangeAddress(1, 1, 15, 16);
		sheet.addMergedRegion(tri3P);
		CellRangeAddress tri4P = new CellRangeAddress(1, 1, 17, 18);
		sheet.addMergedRegion(tri4P);

		String[] cabeceras = new String[] { "Programado", "Ejecutado", "Programado", "Ejecutado", "Programado",
				"Ejecutado", "Programado", "Ejecutado", "% de Avance meta fisica anual", "Programado", "Ejecutado",
				"Programado", "Ejecutado", "Programado", "Ejecutado", "Programado", "Ejecutado",
				"% Ejecucion Presupuesto" };

		for (int i = 1; i < cabeceras.length+1; ++i) {
			String nombrex = cabeceras[i-1];
			HSSFCell cellx = terceroRow.createCell(i + 1);
			cellx.setCellValue(nombrex);
			cellx.setCellStyle(styleBorder);
		}

		int i = 2;
		for (Tarea t : listadoTareaUsuario) {
			HSSFRow dataRow = sheet.createRow(i + 1);
			HSSFCell dataCellId = dataRow.createCell(0);
			dataCellId.setCellValue(t.getIdTarea());
			HSSFCell dataCell = dataRow.createCell(1);
			dataCell.setCellValue(t.getDescripcion());
			dataCell.setCellStyle(styleMetas);
			i++;
		}
		
		HSSFRegionUtil.setBorderBottom(2, metaFisicaMerge, sheet, workbook);
		HSSFRegionUtil.setBorderLeft(2, metaFisicaMerge, sheet, workbook);
		HSSFRegionUtil.setBorderRight(2, metaFisicaMerge, sheet, workbook);
		HSSFRegionUtil.setBorderBottom(2, metaFisicaMerge, sheet, workbook);
		
		HSSFRegionUtil.setBorderBottom(2, metaPrograma, sheet, workbook);
		HSSFRegionUtil.setBorderLeft(2, metaPrograma, sheet, workbook);
		HSSFRegionUtil.setBorderRight(2, metaPrograma, sheet, workbook);
		HSSFRegionUtil.setBorderBottom(2, metaPrograma, sheet, workbook);
		
		HSSFRegionUtil.setBorderBottom(1, tri1F, sheet, workbook);
		HSSFRegionUtil.setBorderLeft(1, tri1F, sheet, workbook);
		HSSFRegionUtil.setBorderRight(1, tri1F, sheet, workbook);
		HSSFRegionUtil.setBorderBottom(1, tri2F, sheet, workbook);
		HSSFRegionUtil.setBorderBottom(1, tri2F, sheet, workbook);
		HSSFRegionUtil.setBorderLeft(1, tri2F, sheet, workbook);
		HSSFRegionUtil.setBorderRight(1, tri2F, sheet, workbook);
		HSSFRegionUtil.setBorderBottom(1, tri2F, sheet, workbook);
		HSSFRegionUtil.setBorderBottom(1, tri3F, sheet, workbook);
		HSSFRegionUtil.setBorderLeft(1, tri3F, sheet, workbook);
		HSSFRegionUtil.setBorderRight(1, tri3F, sheet, workbook);
		HSSFRegionUtil.setBorderBottom(1, tri3F, sheet, workbook);
		HSSFRegionUtil.setBorderBottom(1, tri4F, sheet, workbook);
		HSSFRegionUtil.setBorderLeft(1, tri4F, sheet, workbook);
		HSSFRegionUtil.setBorderRight(1, tri4F, sheet, workbook);
		HSSFRegionUtil.setBorderBottom(1, tri4F, sheet, workbook);
		
		HSSFRegionUtil.setBorderBottom(2, MotivoItem, sheet, workbook);
		HSSFRegionUtil.setBorderLeft(2, MotivoItem, sheet, workbook);
		HSSFRegionUtil.setBorderRight(2, MotivoItem, sheet, workbook);
		HSSFRegionUtil.setBorderBottom(2, MotivoItem, sheet, workbook);

		sheet.setColumnHidden(0,true);
		sheet.setColumnWidth(0, 10);
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		sheet.autoSizeColumn(4);
		sheet.autoSizeColumn(5);
		sheet.autoSizeColumn(6);
		sheet.autoSizeColumn(7);
		sheet.autoSizeColumn(13);
		sheet.autoSizeColumn(14);

		CellUtil.setAlignment(celdaMetaFisica, workbook, CellStyle.ALIGN_CENTER);
		CellUtil.setAlignment(celdaPrograma, workbook, CellStyle.ALIGN_CENTER);
		return workbook;

	}
	

}
