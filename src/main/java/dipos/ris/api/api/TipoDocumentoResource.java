package dipos.ris.api.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dipos.ris.api.domain.TipoDocumento;
import dipos.ris.api.domain.Mensaje;
import dipos.ris.api.service.TipoDocumentoService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/tipoDocumento")
@RequiredArgsConstructor
public class TipoDocumentoResource {
    private final TipoDocumentoService tipoDocumentoService;

    @GetMapping("/listarTipoDocumento")
    @ApiOperation(value = "Este metodo lista todos los tipos de documentos")
    public ResponseEntity<List<TipoDocumento>>getTipoDocumentoes() {
        return ResponseEntity.ok().body(tipoDocumentoService.listarTipoDocumentos());
    }

    @PostMapping("/grabarTipoDocumento")
    @ApiOperation(value = "Este metodo Graba un tipo de documento")
    public ResponseEntity<TipoDocumento>grabarTipoDocumento(@RequestBody TipoDocumento tidoc) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/tipoDocumento/grabarTipoDocumento").toUriString());
        return ResponseEntity.created(uri).body(tipoDocumentoService.grabarTipoDocumento(tidoc));
    }
    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ApiOperation(value = "Este metodo obtiene un tipo de documento")
    @GetMapping(path = "/obtenerTipoDocumentoId/{id}")
	public ResponseEntity<TipoDocumento> obtenerTipoDocumento(@PathVariable(name = "id") int id){
		if(!tipoDocumentoService.tipoDocumentoExiste(id)) 
			return new ResponseEntity(new Mensaje("No existe el tipo de documento"),HttpStatus.NOT_FOUND);
		TipoDocumento tidoc = tipoDocumentoService.obtenerTipoDocumento(id);
		
		return new ResponseEntity<TipoDocumento>(tidoc,HttpStatus.OK);
	}
	
	
	@PutMapping(path = "/actualizarTipoDocumento/{id}")
	public ResponseEntity<TipoDocumento> actualizarTipoDocumento(@PathVariable(name = "id") int id, @RequestBody TipoDocumento tidoc){
		if(!tipoDocumentoService.tipoDocumentoExiste(id))
			return new ResponseEntity<TipoDocumento>(HttpStatus.NOT_FOUND);
		
		TipoDocumento tipoDocumentoActualizado = tipoDocumentoService.obtenerTipoDocumento(id);
		BeanUtils.copyProperties(tidoc, tipoDocumentoActualizado);
		return new ResponseEntity<TipoDocumento>(tidoc,HttpStatus.OK);
		
	}
	
   
}

