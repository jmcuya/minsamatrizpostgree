package dipos.ris.api.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dipos.ris.api.domain.UniOrganica;
import dipos.ris.api.domain.Mensaje;
import dipos.ris.api.service.UniOrganicaService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/uniOrganica")
@RequiredArgsConstructor
public class UniOrganicaResource {
    private final UniOrganicaService uniOrganicaService;

    @GetMapping("/listarUniOrganicasByDirecGeneral/{id}")
    @ApiOperation(value = "Este metodo lista todas las unidades orgánicas dependiendo de la Dirección General seleccionada")
  
    public ResponseEntity<List<UniOrganica>>listarUniOrganicasByDirecGeneral(@PathVariable(name = "id") int id) {
    	  
        //Al enviar 0, que liste todo. Para mostrar todas las UO
        if (id==0) {
            return ResponseEntity.ok().body(uniOrganicaService.listarUniOrganicas());
        }
        else {
        	return ResponseEntity.ok().body(uniOrganicaService.listarUniOrganicasByDirecGeneral(id));
        }
    }

    @PostMapping("/grabarUniOrganica")
    @ApiOperation(value = "Este metodo Graba una unidad orgánica")
    public ResponseEntity<UniOrganica>grabarUniOrganica(@RequestBody UniOrganica uor) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/uniOrganica/grabarUniOrganica").toUriString());
        return ResponseEntity.created(uri).body(uniOrganicaService.grabarUniOrganica(uor));
    }
    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ApiOperation(value = "Este metodo obtiene una unidad orgánica")
    @GetMapping(path = "/obtenerUniOrganicaId/{id}")
	public ResponseEntity<UniOrganica> obtenerUniOrganica(@PathVariable(name = "id") int id){
		if(!uniOrganicaService.uniOrganicaExiste(id)) 
			return new ResponseEntity(new Mensaje("No existe la unidad orgánica"),HttpStatus.NOT_FOUND);
		UniOrganica uor = uniOrganicaService.obtenerUniOrganica(id);
		
		return new ResponseEntity<UniOrganica>(uor,HttpStatus.OK);
	}
	
	
	@PutMapping(path = "/actualizarUniOrganica/{id}")
	public ResponseEntity<UniOrganica> actualizarUniOrganica(@PathVariable(name = "id") int id, @RequestBody UniOrganica uor){
		if(!uniOrganicaService.uniOrganicaExiste(id))
			return new ResponseEntity<UniOrganica>(HttpStatus.NOT_FOUND);
		
		UniOrganica uniOrganicaActualizado = uniOrganicaService.obtenerUniOrganica(id);
		BeanUtils.copyProperties(uor, uniOrganicaActualizado);
		return new ResponseEntity<UniOrganica>(uor,HttpStatus.OK);
		
	}
	
   
}

