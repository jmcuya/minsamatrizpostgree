package dipos.ris.api.api;

import java.net.URI;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dipos.ris.api.domain.DirecGeneral;
import dipos.ris.api.domain.Mensaje;
import dipos.ris.api.service.DirecGeneralService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/direcGeneral")
@RequiredArgsConstructor
public class DirecGeneralResource {
    private final DirecGeneralService direcGeneralService;

    @GetMapping("/listarDirecGeneral")
    @ApiOperation(value = "Este metodo lista todas las direcciones generales dependiendo de la unidad orgánica seleccionada")
    public ResponseEntity<List<DirecGeneral>>getDirecGenerales() {
        return ResponseEntity.ok().body(direcGeneralService.listarDirecGenerales());
    }

    @PostMapping("/grabarDirecGeneral")
    @ApiOperation(value = "Este metodo Graba una Dirección General")
    public ResponseEntity<DirecGeneral>grabarDirecGeneral(@RequestBody DirecGeneral dirgen) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/direcGeneral/grabarDirecGeneral").toUriString());
        return ResponseEntity.created(uri).body(direcGeneralService.grabarDirecGeneral(dirgen));
    }
    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ApiOperation(value = "Este metodo obtiene una Dirección General")
    @GetMapping(path = "/obtenerDirecGeneralId/{id}")
	public ResponseEntity<DirecGeneral> obtenerDirecGeneral(@PathVariable(name = "id") int id){
		if(!direcGeneralService.direcGeneralExiste(id)) 
			return new ResponseEntity(new Mensaje("No existe la Dirección General"),HttpStatus.NOT_FOUND);
		DirecGeneral dirgen = direcGeneralService.obtenerDirecGeneral(id);
		
		return new ResponseEntity<DirecGeneral>(dirgen,HttpStatus.OK);
	}
	
	
	@PutMapping(path = "/actualizarDirecGeneral/{id}")
	public ResponseEntity<DirecGeneral> actualizarDirecGeneral(@PathVariable(name = "id") int id, @RequestBody DirecGeneral dirgen){
		if(!direcGeneralService.direcGeneralExiste(id))
			return new ResponseEntity<DirecGeneral>(HttpStatus.NOT_FOUND);
		
		DirecGeneral direcGeneralActualizado = direcGeneralService.obtenerDirecGeneral(id);
		BeanUtils.copyProperties(dirgen, direcGeneralActualizado);
		return new ResponseEntity<DirecGeneral>(dirgen,HttpStatus.OK);
		
	}
	
   
}

