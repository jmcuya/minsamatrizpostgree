package dipos.ris.api.api;


import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import dipos.ris.api.request.UsuarioRequest;
import dipos.ris.api.response.DevolverGrillaUsuariosDto;
import dipos.ris.api.response.UsuarioResponse;
import dipos.ris.api.service.UsuarioService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/usuario")
@RequiredArgsConstructor
@Slf4j
public class UsuarioResource {
    private final UsuarioService usuarioService;

    @GetMapping("/listarUsuario")
    @ApiOperation(value = "Este metodo lista todos los usuarios")
    public ResponseEntity<List<UsuarioResponse>>getUsers() {
    	List<UsuarioResponse>listado = new ArrayList<UsuarioResponse>();
    	try {
			listado=usuarioService.listarUsuarios();
    		
		} catch (Exception e) {
			log.error("Error getUsers:" + ExceptionUtils.getFullStackTrace(e));
			return ResponseEntity.internalServerError().body(null);
		}
		return ResponseEntity.ok().body(listado);
        
    }

    @PostMapping("/devolverGrillaUsuariosDto")
    @ApiOperation(value = "Este metodo lista todos los usuarios para el buscador")
	public ResponseEntity<DevolverGrillaUsuariosDto> listarUsuarios(@RequestBody UsuarioRequest req) {
    	PageRequest pageable = PageRequest.of(req.getPage(), req.getCantItemaPagina());
    	DevolverGrillaUsuariosDto respuestaUsuarios = null;
    	
    	try {
    		respuestaUsuarios = usuarioService.listarGrillaUsuarios(req, (Pageable) pageable);

			return new ResponseEntity<>(respuestaUsuarios, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(respuestaUsuarios, HttpStatus.NOT_FOUND);
		}
    	
    }
    
    @PostMapping("/grabarUsuario")
    @ApiOperation(value = "Este metodo Graba un usuario")
    public ResponseEntity<UsuarioResponse>grabarUsuario(@RequestBody UsuarioRequest user) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/usuario/grabarUsuario").toUriString());
        UsuarioResponse userResponse = new UsuarioResponse();
        try {
        	userResponse= usuarioService.grabarUsuario(user);
		} catch (Exception e) {
			log.error("grabarUsuario error: "+ ExceptionUtils.getFullStackTrace(e));
		}
        return ResponseEntity.created(uri).body(userResponse);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	@ApiOperation(value = "Este metodo obtiene un usuario")
    @GetMapping(path = "/obtenerUsuarioId/{id}")
	public ResponseEntity<UsuarioResponse> obtenerUsuario(@PathVariable(name = "id") int id){
		if(!usuarioService.usuarioExiste(id)) 
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		UsuarioResponse user = usuarioService.obtenerUsuario(id);
		
		return new ResponseEntity<UsuarioResponse>(user,HttpStatus.OK);
	}
	
	@PutMapping(path = "/actualizarUsuario/{id}")
	@ApiOperation(value = "Este metodo actualiza un usuario")
	@CrossOrigin(origins = "http://localhost:4200")
	public ResponseEntity<UsuarioResponse> actualizaUsuario(@PathVariable(name = "id") int id, @RequestBody UsuarioRequest user){
		if(!usuarioService.usuarioExiste(id))
			return new ResponseEntity<UsuarioResponse>(HttpStatus.NOT_FOUND);
		
		UsuarioResponse usuarioActualizado = usuarioService.grabarUsuario(user);
		return new ResponseEntity<UsuarioResponse>(usuarioActualizado,HttpStatus.OK);
		
	}	
   
}

