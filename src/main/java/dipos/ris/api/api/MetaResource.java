package dipos.ris.api.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dipos.ris.api.domain.Meta;
import dipos.ris.api.request.MetaRequest;
import dipos.ris.api.request.MetaRequestSave;
import dipos.ris.api.request.ValidaTareaRequest;
import dipos.ris.api.response.BandejaRegistrosMetaResponse;
import dipos.ris.api.response.MetaAnualResponse;
import dipos.ris.api.response.MetaResponse;
import dipos.ris.api.service.MetaService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/meta")
@RequiredArgsConstructor
public class MetaResource {
    private final MetaService metaService;

    @PostMapping("/grabarMeta")
    @ApiOperation(value = "Este metodo Graba la meta (lista de actividades y avance del usuario)")
    public ResponseEntity<MetaResponse>grabarMeta(@RequestBody MetaRequestSave req) {
    	MetaResponse metaResponse = metaService.grabarMeta(req);
        return new ResponseEntity<MetaResponse>(metaResponse,HttpStatus.OK);
    }

	@ApiOperation(value = "Este metodo obtiene una meta")
    @PostMapping(path = "/listarMetas")
	public ResponseEntity<MetaAnualResponse> listarMetas(@RequestBody MetaRequest req){		
		MetaAnualResponse lista = metaService.listaMetas(req);		
		return new ResponseEntity<MetaAnualResponse>(lista,HttpStatus.OK);
	}
		

	@ApiOperation(value = "Este metodo obtiene una meta")
    @PostMapping(path = "/listarMetasConEvidencia")
	public ResponseEntity<BandejaRegistrosMetaResponse> listarMetasConEvidencia(@RequestBody MetaRequest req){		
		BandejaRegistrosMetaResponse lista = metaService.listaBandejaMetas(req);		
		return new ResponseEntity<BandejaRegistrosMetaResponse>(lista,HttpStatus.OK);
	}
	
	@PutMapping(path = "/actualizarMeta/{idUsuario}/{anio}")
	public ResponseEntity<Meta> actualizarMeta(@PathVariable(name = "idUsuario") int idUsuario, @PathVariable(name = "anio") String anio, @RequestBody Meta met){
//		if(!metaService.metaExiste(idUsuario, anio))
//			return new ResponseEntity<Meta>(HttpStatus.NOT_FOUND);
//		
//		Meta metaActualizado = metaService.obtenerMeta(idUsuario, anio);
//		BeanUtils.copyProperties(met, metaActualizado);
		return new ResponseEntity<Meta>(met, HttpStatus.OK);
		
	}
	
	@PostMapping(path = "/validarTarea")
	public ResponseEntity<MetaResponse> validarTarea(@RequestBody ValidaTareaRequest req){
		MetaResponse metaResponse =	metaService.validarTarea(req);
		return new ResponseEntity<MetaResponse>(metaResponse, HttpStatus.OK);
		
	}
	
   
}

