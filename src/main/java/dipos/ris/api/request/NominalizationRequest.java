package dipos.ris.api.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class NominalizationRequest implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6868161114411256427L;
	private String dni;
    private String edad;
    private String genero;
    private String materno;
    private String nombres;
    private String option;
    private String paterno;
    private String quick;
    private RisRequest ris;

    
}
