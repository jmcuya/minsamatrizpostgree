package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor

@Table(name = "tipodocumento")
public class TipoDocumento implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -699893532241796580L;
	@Id 
	@SequenceGenerator(name="idSequence", sequenceName="seq_tipodocumento",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "idtipodocumento")
    private int idTipoDocumento;
    private String nombre;
    private int estado;
}
