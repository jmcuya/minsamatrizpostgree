package dipos.ris.api.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "evidencia")
public class Evidencia implements  Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6645139897749122470L;
	@Id 
	@SequenceGenerator(name="idSequence",    sequenceName="seq_evidencia",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "idevidencia")
    private int idEvidencia;
    private String asunto;
    @Column(name = "fechaemision")
    private Date fechaEmision;
    private int estado=1;
    private String fileUrl;    
    
    @ManyToOne
	@JoinColumn(name="idMeta")
    private Meta meta;
    @ManyToOne
	@JoinColumn(name="idTipoDocEvidencia")
    private TipoDocEvidencia tipoDocEvidencia; 

}
