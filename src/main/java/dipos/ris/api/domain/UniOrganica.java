package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor

@Table(name = "uniorganica")
public class UniOrganica implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4673268065833149561L;
	@Id 
	@SequenceGenerator(name="idSequence", sequenceName="seq_uniorganica",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "iduniorganica")
    private int idUniOrganica;
	private String nombre;
    private int estado;
    
    @ManyToOne
	@JoinColumn(name="iddirecgeneral")
    private DirecGeneral direcGeneral;
}
