package dipos.ris.api.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "user", schema = "public")
public class User implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -9195116294310645727L;
	@Id 
	@SequenceGenerator(name="idSequence", sequenceName="seq_user",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "id")
    private int id;
    private String name;
    private String username;
    private String password;
    @ManyToMany(targetEntity = Role.class)
    private Collection<Role> roles;
}
