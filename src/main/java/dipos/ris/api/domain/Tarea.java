package dipos.ris.api.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "tarea")
@JsonIgnoreProperties({"listadoTareaUsuario"})
public class Tarea implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7412260146291944778L;
	@Id 
	@SequenceGenerator(name="idSequence", sequenceName="seq_tarea",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "idtarea")
    private int idTarea;
    private String descripcion;
    private int estado; 
    
    @JsonIgnore
    @OneToMany(mappedBy="tarea",fetch=FetchType.LAZY)
	private List<TareaUsuario> listadoTareaUsuario;

}
