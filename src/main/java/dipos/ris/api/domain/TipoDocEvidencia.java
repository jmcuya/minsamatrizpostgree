package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "tipoDocEvidencia")
public class TipoDocEvidencia implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6246558382999610922L;
	@Id 
	@SequenceGenerator(name="idSequence", sequenceName="seq_tipodocevidencia",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "idtipodocevidencia")
    private int idTipoDocEvidencia;
    private String nombre;
    private int estado;    

}
