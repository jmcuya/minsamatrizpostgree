package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "meta")
public class Meta implements  Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 3446038476502089767L;
	@Id 
	@SequenceGenerator(name="idSequence",    sequenceName="seq_meta",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "idmeta")
    private int idMeta;             
    private String anio   ;            
    private String tipo   ;   
	@Column(name = "valortrim1prog")         
    private int valorTrim1Prog;
	@Column(name = "valortrim1ejec")
    private int valorTrim1Ejec  ; 
	@Column(name = "valortrim2prog") 
    private int valorTrim2Prog ;  
	@Column(name = "valortrim2ejec")  
    private int valorTrim2Ejec ;  
	@Column(name = "valortrim3prog")  
    private int valorTrim3Prog;  
	@Column(name = "valortrim3ejec")  
    private int valorTrim3Ejec ; 
	@Column(name = "valortrim4prog")   
    private int valorTrim4Prog ; 
	@Column(name = "valortrim4ejec")   
    private int valorTrim4Ejec ;    
    private String estado ;            
    private String motivo  ;   
	@Column(name = "motivorechazo")        
    private String motivoRechazo  ;          
    
    
    @ManyToOne
	@JoinColumn(name="idTarea")
    private Tarea tarea;
    @ManyToOne
	@JoinColumn(name="idUsuario")
    private Usuario usuario; 

}
