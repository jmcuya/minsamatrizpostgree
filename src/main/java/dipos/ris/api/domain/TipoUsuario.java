package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor

@Table(name = "tipousuario")
public class TipoUsuario implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4974635057368121878L;
	@Id 
	@SequenceGenerator(name="idSequence", sequenceName="seq_tipousuario",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "idtipousuario")
    private int idTipoUsuario;
	@Column(name = "tipousuario")
    private String TipoUsuario;
    private int estado;
}
