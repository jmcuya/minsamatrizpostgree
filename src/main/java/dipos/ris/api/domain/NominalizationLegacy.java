package dipos.ris.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import javax.persistence.*;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "nominalizacionlegacy")
public class NominalizationLegacy implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7916850903947691881L;

	@Column(name = "idmanzana")
    private String idmanzana;

    @Column(name = "cod_reni")
    private String cod_reni;

    @Column(name = "ipress")
    private String ipress;

    @Column(name = "id_ris")
    private String id_ris;

    @Column(name = "ris")
    private String ris;

    @Id
    @Column(name="dni", length=9, unique=true, nullable=false)
    private String dni;

    @Column(name = "nombres_ref")
    private String nombres_ref;

    @Column(name = "paterno_ref")
    private String paterno_ref;

    @Column(name = "materno_ref")
    private String materno_ref;

    @Column(name = "genero")
    private String genero;

    @Column(name = "edad")
    private String edad;

    @Column(name = "longitud")
    private String longitud;

    @Column(name = "latitud")
    private String latitud;

}

