package dipos.ris.api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "condlaboral")
public class CondLaboral implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1070097100783011945L;
	
	@Id 
	@SequenceGenerator(name="idSequence",  sequenceName="seq_condLaboral",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "idcondlaboral")
    private int idCondLaboral;
	
	@Column(name = "nombre")
    private String nombre;
	@Column(name = "estado")
    private int estado;
}
