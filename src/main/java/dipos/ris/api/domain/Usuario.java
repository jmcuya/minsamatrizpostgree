package dipos.ris.api.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "usuario")
@JsonIgnoreProperties({"listadoTareaUsuario"})
public class Usuario implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2230432808811704866L;
	@Id 
	@SequenceGenerator(name="idSequence", sequenceName="seq_usuario",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "idusuario")
    private int idUsuario;
	@Column(name = "nrodocumento")
    private String nroDocumento;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apepaterno")
    private String apePaterno;
    @Column(name = "apematerno")
    private String apeMaterno;
    @Column(name = "fecnacimiento")
    private Date fecNacimiento;
    private String sexo;
    @Column(name = "usuario")
    private String usuario;
    @Column(name = "password")
    private String password;
    @Column(name = "estado")
    private int estado;
    
    @ManyToOne
	@JoinColumn(name="idcargo")
    private Cargo cargo;
    
    @ManyToOne
	@JoinColumn(name="idcondlaboral")
    private CondLaboral condLaboral;
    
    @ManyToOne
	@JoinColumn(name="iduniorganica")
    private UniOrganica uniOrganica;
    
    @ManyToOne
	@JoinColumn(name="idtipousuario")
    private TipoUsuario tipoUsuario;
    
    @ManyToOne
	@JoinColumn(name="idtipodocumento")
    private TipoDocumento tipoDocumento;
    
    @ManyToOne
	@JoinColumn(name="idrole")
    private Role role;
  
    @JsonIgnore
    @OneToMany(mappedBy="usuario",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<TareaUsuario> listadoTareaUsuario;
  
}
