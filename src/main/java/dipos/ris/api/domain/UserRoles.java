package dipos.ris.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_roles")
public class UserRoles implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3112055484335524689L;
	@Id 
	@SequenceGenerator(name="idSequence", sequenceName="seq_user_roles",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "iduserroles")
    private int idUserRoles;
	@Column(name = "iduser")
    private User user;
	@Column(name = "idrol")
    private Role role;
}
