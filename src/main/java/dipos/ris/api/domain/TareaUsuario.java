package dipos.ris.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Where;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "tareausuario")
@Where(clause = "estado=1")
public class TareaUsuario implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1542026505046500757L;
	@Id 
	@SequenceGenerator(name="idSequence", sequenceName="seq_tareausuario",allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="idSequence")
	@Column(name = "idtareausuario")
    private int idTareaUsuario;
    
    @ManyToOne
	@JoinColumn(name="idtarea")
    private Tarea tarea;
    
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idusuario")
    private Usuario usuario; 
    
    private Integer anio;
    
    private int estado=1;
    
}
