package dipos.ris.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@PropertySource(value = "classpath:application.properties")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	//@Value("${sistema.ruta.cors}")
	private String cors="*";
	
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/api/**").allowedOrigins(cors).allowedMethods("*").allowedHeaders("*");
			}
		};
	}
	
	/*
	@Bean
	CommandLineRunner run(UserService userService) {
		return args -> {
			userService.saveRole(new Role(null, "ROLE_USER"));
			userService.saveRole(new Role(null, "ROLE_MANAGER"));
			userService.saveRole(new Role(null, "ROLE_ADMIN"));
			userService.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));

			userService.saveUser(new User(null, "Nick Alvi", "NAlvi", "12345", new ArrayList<>()));
			userService.saveUser(new User(null, "Silvia Saravia", "SSaravia", "12345", new ArrayList<>()));
			userService.saveUser(new User(null, "Marita Pereyra", "MPereyra", "12345", new ArrayList<>()));
			userService.saveUser(new User(null, "Miguel Tancun", "MTancun", "12345", new ArrayList<>()));

			userService.addRoleToUser("NAlvi","ROLE_SUPER_ADMIN");
			userService.addRoleToUser("NAlvi","ROLE_USER");
			userService.addRoleToUser("SSaravia","ROLE_MANAGER");
			userService.addRoleToUser("MPereyra","ROLE_MANAGER");
			userService.addRoleToUser("MTancun","ROLE_SUPER_ADMIN");
			userService.addRoleToUser("MTancun","ROLE_ADMIN");
			userService.addRoleToUser("MTancun","ROLE_MANAGER");
		};
	}*/
	
	
}
