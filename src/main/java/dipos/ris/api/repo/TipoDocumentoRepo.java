package dipos.ris.api.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.TipoDocumento;

@Repository
public interface TipoDocumentoRepo extends CrudRepository<TipoDocumento, Integer>{

}
