package dipos.ris.api.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.TipoUsuario;

@Repository
public interface TipoUsuarioRepo extends CrudRepository<TipoUsuario, Integer>{

}
