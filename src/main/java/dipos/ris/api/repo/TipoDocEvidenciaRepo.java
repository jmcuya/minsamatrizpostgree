package dipos.ris.api.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.TipoDocEvidencia;

@Repository
public interface TipoDocEvidenciaRepo extends CrudRepository<TipoDocEvidencia, Integer>{

}
