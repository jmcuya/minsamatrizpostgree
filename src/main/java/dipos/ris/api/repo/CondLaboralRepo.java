package dipos.ris.api.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.CondLaboral;

@Repository
public interface CondLaboralRepo extends CrudRepository<CondLaboral, Integer>{

}
