package dipos.ris.api.repo;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.Usuario;

@Repository
public interface UsuarioRepo extends 	CrudRepository<Usuario, Integer>,
										PagingAndSortingRepository<Usuario, Integer>,
										JpaRepository<Usuario, Integer>{

	@Query("select o from Usuario o "
			+ " join o.tipoDocumento td"
			+ " join o.uniOrganica c "			
			+ " join o.cargo e"
			+ " join o.condLaboral f"
			+ " where o.estado = 1 and c.estado = 1 and e.estado = 1 and f.estado = 1 "
			+ " and (:pIdUniOrganica 	= 0 OR o.uniOrganica.idUniOrganica = :pIdUniOrganica) "
			+ " and (:pNombre 			is NULL OR o.nombre = :pNombre) "
			+ " and (:pApePaterno 		is NULL OR o.apePaterno = :pApePaterno) "
			+ " and (:pApeMaterno 		is NULL OR o.apeMaterno = :pApeMaterno) "
			+ " and (:pIdTipoDocumento 	= 0 OR td.idTipoDocumento = :pIdTipoDocumento) "
			+ " and (:pNroDocumento 	is NULL OR o.nroDocumento = :pNroDocumento) "			
			+ " and (:pIdCargo 			= 0 OR e.idCargo = :pIdCargo) "
			+ " and (:pIdCondLaboral 	= 0 OR f.idCondLaboral = :pIdCondLaboral) "
			/**/
			)
	public Page<Usuario> listarUsuariosBuscador(
			@Param("pIdUniOrganica") int idUniOrganica, 
			@Param("pNombre") String nombre, 
			@Param("pApePaterno") String apePaterno, 
			@Param("pApeMaterno") String apeMaterno, 
			@Param("pIdTipoDocumento") int idTipoDocumento, 
			@Param("pNroDocumento") String nroDocumento, 
			@Param("pIdCargo") int idCargo, 
			@Param("pIdCondLaboral") int idCondLaboral,
			
			Pageable pageable
			);	
	
}
