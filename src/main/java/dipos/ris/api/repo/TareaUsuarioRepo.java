package dipos.ris.api.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.TareaUsuario;

@Repository
public interface TareaUsuarioRepo extends CrudRepository<TareaUsuario, Integer>{

}
