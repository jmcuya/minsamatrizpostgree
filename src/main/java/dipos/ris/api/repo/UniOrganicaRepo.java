package dipos.ris.api.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import dipos.ris.api.domain.UniOrganica;

@Repository
public interface UniOrganicaRepo extends CrudRepository<UniOrganica, Integer>{

	@Query(" select o from UniOrganica o "
			+ " join o.direcGeneral d where d.idDirecGeneral=:id")
	List<UniOrganica> listarPorDirecGeneral(@Param("id") int id);
	
	@Query(" select o from UniOrganica o where o.estado = 1")
	List<UniOrganica> listarUniOrganicas();

}
